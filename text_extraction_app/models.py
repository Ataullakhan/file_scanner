# from __future__ import unicode_literals
#
# from django.db import models
#
#
# class Document(models.Model):
#     description = models.CharField(max_length=255, blank=True)
#     document = models.FileField(upload_to='documents/')
#     uploaded_at = models.DateTimeField(auto_now_add=True)
#
#     class Meta:
#         db_table = "document"
#
#
# class Detailworks(models.Model):
#     detail_work = models.CharField(max_length=255)
#     matched_text_from_given = models.CharField(max_length=255)
#     matched_text_from_uploded = models.CharField(max_length=255)
#     status = models.CharField(max_length=255, default='')
#
#     class Meta:
#         db_table = "detailworks"
#
#
# class AnnualTurnover(models.Model):
#     client = models.CharField(max_length=255)
#     year = models.CharField(max_length=255)
#     turnover = models.CharField(max_length=255, default='')
#     pdf_path = models.FileField(max_length=255)
#
#     class Meta:
#         db_table = "annualtrunover"
#
#
# class AnnualTurnoverValidation(models.Model):
#     annualTurnover = models.ForeignKey(AnnualTurnover, on_delete=models.CASCADE)
#     matched_text_from_given = models.CharField(max_length=255)
#     matched_text_from_uploded = models.CharField(max_length=255)
#     status = models.CharField(max_length=255, default='')
#
#     class Meta:
#         db_table = "annualTurnoverValidation"
#
#
# class Technology(models.Model):
#     technology = models.CharField(max_length=255)
#     matched_text_from_given = models.CharField(max_length=255)
#     matched_text_from_uploded = models.CharField(max_length=255)
#     status = models.CharField(max_length=255, default='')
#
#     class Meta:
#         db_table = "technology"