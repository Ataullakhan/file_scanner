import json
import os

import requests
from django.shortcuts import render, redirect
from django.conf import settings
from django.core.files.storage import FileSystemStorage

# from uploads.core.models import Document
# from uploads.core.forms import DocumentForm
from pdf2image import convert_from_path
from PIL import Image
import pytesseract
import re
from tika import parser
from django.db import connection
import pandas as pd
import io
import itertools
# import tesserocr
# from wand.image import Image as Img
# from text_extraction_app.forms import DocumentForm
from text_extraction_app.models import *
from text_extraction_app.query import get_query
from text_extraction_app.utils import get_result
from collections import Counter
from uploads.utils import ocr_space_file
# from wand.image import Image as Img
# import subprocess
# import convertapi
# from wand.image import Image
# from wand.color import Color
import textract

path = settings.MEDIA_ROOT


def getlowerdf(df):
    """
    function for fetch and manage columns
    :param df:
    :return:
    """
    cols = df.columns
    cols_dict = {}
    for item in cols:
        cols_dict[item] = item.lower()
    df.rename(columns=cols_dict, inplace=True)
    return df


def getdata(qry, todict=False, single=False):
    """
    function for fetch querys and stabilised connection with datatables
    :param qry:
    :param todict:
    :param single:
    :return:
    """
    data = pd.read_sql(qry, connection)
    # connection.close()
    data = getlowerdf(data)
    data = data.fillna(0)
    if todict:
        data = data.to_dict(orient="records")
        if single:
            data = data[0]
    return data


def home(request):
    return render(request, 'home.html')


##################################################################################################
# TO-DO Try this PYOCR insted of existing pyteseract and apachi api......
# from PIL import Image
# >>> import sys
# >>>
# >>> import pyocr
# >>> import pyocr.builders
#
#  txt = tool.image_to_string(Image.open('test4.png'),builder=pyocr.builders.TextBuilder())
##################################################################################################


def simple_upload(request):
    """

    :param request:
    :return:
    """
    # if request.method == 'POST':
    try:
        fatch_info = "select * from turnover"
        response = getdata(fatch_info)
        if response.empty:
            text1 =""
            # for k in range(1, 66):
            #     a = "pdfCount+++++", k
            #     print(a)
            pages3 = convert_from_path(path + '/documents/riteslimited2016-17_7_1.pdf', 500)[0]
            # limit = 10
            #     for page in pages3:
            # for i in range(1, 10):
            pages3.save(path + '/converted_png/Experience.png', 'png')
        # print(index)
        # if index == limit:
        #     break
    #     # third file extract
            file3 = path + '/converted_png/Experience.png'
            file_data3 = pytesseract.image_to_string(Image.open(file3), lang='eng')
            # # Get files text content
            text1 += file_data3
            turnover_text1_pdf1 = ''
            annual_turnover_year_pdf_text = re.finditer(r'(\bTOTAL REVENUE\b)(.*)', text1)
            for client_txt in annual_turnover_year_pdf_text:
                turnover_text1_pdf1 = client_txt.group()
                turnover_text1_pdf1 = turnover_text1_pdf1.lstrip('TOTAL REVENUE')
                turnover_text1_pdf1 = turnover_text1_pdf1.replace("1,277.61","")
                turnover_text1_pdf1 = turnover_text1_pdf1.replace('\n','')
                turnover_text1_pdf1 = turnover_text1_pdf1.replace('\t','')

            turnover_text2_pdf1 = ''
            annual_turnover_pdf_text = re.finditer(r'(\bPARTICULARS NOTE N0\b)(.*)',text1)
            for client_txt in annual_turnover_pdf_text:
                turnover_text2_pdf1 = client_txt.group()
                turnover_text2_pdf1 = turnover_text2_pdf1.lstrip('PARTICULARS NOTE N0')
                turnover_text2_pdf1 = turnover_text2_pdf1.replace('\n','')
                turnover_text2_pdf1 = turnover_text2_pdf1.replace('\t','')

            undertaking_text1 = ''
            annual_turnover_pdf_text = re.finditer(r'(\bundertaking|\bUNDERTAKING\b)(.*)', text1)
            for client_txt in annual_turnover_pdf_text:
                undertaking_text1 = client_txt.group()

            if(undertaking_text1 == ""):
                undertaking_text1 = "Client Certified"


            #####################################
            # secound pdf

            # annual_turnover_data = getdata(get_query("annual_turnover"))
            # file = annual_turnover_data['pdf_path']

            text2 = ""
            pages3 = convert_from_path(path + '/documents/riteslimited2017-18_8_1.pdf', 500)[0]
            limit = 10
            # for index, page in enumerate(pages3):
            pages3.save(path + '/converted_png/Experience.png', 'png')
            # print(index)
            # if index == limit:
            #     break
            #     # third file extract
            file3 = path + '/converted_png/Experience.png'
            #     # Parse data from file
            # file_data3 = parser.from_file(file3)
            file_data3 = pytesseract.image_to_string(Image.open(file3), lang='eng')
            # Get files text content
            text2 += file_data3
            # text2 += file_data3['content']

            turnover_text1_pdf2 = ''
            annual_turnover_year_pdf_text = re.finditer(r'(\bTOTAL REVENUE\b)(.*)', text2)
            for client_txt in annual_turnover_year_pdf_text:
                turnover_text1_pdf2 = client_txt.group()
                turnover_text1_pdf2 = turnover_text1_pdf2.lstrip('TOTAL REVENUE')
                turnover_text1_pdf2 = turnover_text1_pdf2.replace("1,506.80", "")
                turnover_text1_pdf2 = turnover_text1_pdf2.replace('\n','')
                turnover_text1_pdf2 = turnover_text1_pdf2.replace('\t','')
            turnover_text2_pdf2 = ''
            annual_turnover_pdf_text = re.finditer(r'(\bPARTICULARS NOTE NO. YEAR ENDED YEAR ENDED\b)((.*\n){2})', text2)
            for client_txt in annual_turnover_pdf_text:
                turnover_text2_pdf2 = client_txt.group()
                turnover_text2_pdf2 = turnover_text2_pdf2.lstrip('PARTICULARS NOTE NO. YEAR ENDED YEAR ENDED')
                turnover_text2_pdf2 = turnover_text2_pdf2.replace('\n','')
                turnover_text2_pdf2 = turnover_text2_pdf2.replace('\t','')

            undertaking_text2 = ''
            annual_turnover_pdf_text = re.finditer(r'(\bundertaking|\bUNDERTAKING\b)(.*)', text2)
            for client_txt in annual_turnover_pdf_text:
                undertaking_text2 = client_txt.group()

            if (undertaking_text2 == ""):
                undertaking_text2 = "Client Certified"

            # annual_turnover_data = getdata(get_query("annual_turnover"))
            Sno = ['7', '8']
            # client_list_given = ['RITES LIMITED (Sno.7)', 'RITES LIMITED (Sno.8)']
            year_list_given = ['2016-17 Calendar Year:2016', '2017-18 Calendar Year:2017']
            tunover_list_given = ['1506.8', '1602.58']
            document = ['documents/riteslimited2016-17_7_1.pdf',
                        'documents/riteslimited2017-18_8_1.pdf']
            supporing_document = ['N/A', 'N/A']

            annual_detail_year_list = [turnover_text2_pdf1, turnover_text2_pdf2]
            annual_detail_turnover_list = [turnover_text1_pdf1, turnover_text1_pdf2 ]

            undertaking_text = [undertaking_text1, undertaking_text2]

            status_year = []
            status_turnover = []
            for j, k in zip(year_list_given, annual_detail_year_list):
                    cosine_sim = get_result(j, k)
                    if cosine_sim >= 0.1:
                        status_year.append("Matched")
                        status_year.append(k)
                    else:
                        status_year.append("Not matched")
                        status_year.append(k)
            for j, k in zip(tunover_list_given, annual_detail_turnover_list):
                    cosine_sim = get_result(j, k)
                    if cosine_sim >= 0.1:
                        status_turnover.append("Matched")
                        status_turnover.append(k)
                    else:
                        status_turnover.append("Not matched")
                        status_turnover.append(k)

            status_year_1 = status_year[0::2]

            # count_turnover_year_status1 = status_year_1 #Counter(status_year_1).most_common()
            status_turnover_1 = status_turnover[0::2]
            # count_turnover_status2 = [status_turnover_1.count(x) for x in set(status_turnover_1)] #Counter(status_turnover_1).most_common()

            status_year_2 = status_year[1::2]
            status_turnover_2 = status_turnover[1::2]

            turn_over_zipall = zip(Sno, year_list_given,
                                   tunover_list_given, status_year_1, status_turnover_1, document, supporing_document)

            status_year_3 = list(map(lambda s: s.strip(), status_year_2))

            status_turnover_3 = list(map(lambda s: s.strip(), status_turnover_2))

            count_turnover_year_status1 = []
            count_turnover_status2 = []
            for Sno, year_list_given, tunover_list_given, status_year_1, status_turnover_1, document, supporing_document, annual_detail_year_list, status_year_2, status_turnover_2 in zip(Sno, year_list_given, tunover_list_given, status_year_1, status_turnover_1, document, supporing_document, annual_detail_year_list, status_year_2, status_turnover_2):

                params = {
                    'Sno' : Sno,
                    'year_list_given' : year_list_given,
                    'tunover_list_given' : tunover_list_given ,
                    'status_year_1' : status_year_1,
                    'status_turnover_1' : status_turnover_1,
                    'document' : document ,
                    'supporing_document' : supporing_document,
                    'annual_detail_year_list' : annual_detail_year_list,
                    'status_year_2' : status_year_2,
                    'status_turnover_2' : status_turnover_2
                }

                insert_query = "insert into turnover(Sno, year_list_given, tunover_list_given, status_year_1," \
                               " status_turnover_1, document, supporing_document, " \
                               " annual_detail_year_list, status_year_2, status_turnover_2)" \
                                           + " VALUES({Sno}, '{year_list_given}', '{tunover_list_given}', '{status_year_1}'," \
                               " '{status_turnover_1}', '{document}', '{supporing_document}', " \
                               " '{annual_detail_year_list}', '{status_year_2}', '{status_turnover_2}')".format(**params)


                cursor = connection.cursor()
                cursor.execute(insert_query)
                fatch_info = "select * from turnover"
                response = getdata(fatch_info)
                Sno = response['sno'].values.tolist()
                year_list_given = response['year_list_given'].values.tolist()
                tunover_list_given = response['tunover_list_given'].values.tolist()
                status_year_1 = response['status_year_1'].values.tolist()
                status_turnover_1 = response['status_turnover_1'].values.tolist()
                document = response['document'].values.tolist()
                supporing_document = response['supporing_document'].values.tolist()
                annual_detail_year_list = response['annual_detail_year_list'].values.tolist()
                status_year_2 = response['status_year_2'].values.tolist()
                status_turnover_2 = response['status_turnover_2'].values.tolist()

                count_turnover_year_status1 = Counter(status_year_1).most_common()
                count_turnover_status2 =  Counter(status_turnover_1).most_common()
                turn_over_zipall = zip(Sno, year_list_given,
                                       tunover_list_given, status_year_1, status_turnover_1, document, supporing_document)

                status_year_3 = list(map(lambda s: s.strip(), status_year_2))

                status_turnover_3 = list(map(lambda s: s.strip(), status_turnover_2))
        else:
            fatch_info = "select * from turnover"
            response = getdata(fatch_info)
            Sno = response['sno'].values.tolist()
            year_list_given = response['year_list_given'].values.tolist()
            tunover_list_given = response['tunover_list_given'].values.tolist()
            status_year_1 = response['status_year_1'].values.tolist()
            status_turnover_1 = response['status_turnover_1'].values.tolist()
            document = response['document'].values.tolist()
            supporing_document = response['supporing_document'].values.tolist()
            annual_detail_year_list = response['annual_detail_year_list'].values.tolist()
            status_year_2 = response['status_year_2'].values.tolist()
            status_turnover_2 = response['status_turnover_2'].values.tolist()

            count_turnover_year_status1 = Counter(status_year_1).most_common()
            count_turnover_status2 = Counter(status_turnover_1).most_common()

            turn_over_zipall = zip(Sno, year_list_given,
                                   tunover_list_given, status_year_1, status_turnover_1, document, supporing_document)

            status_year_3 = list(map(lambda s: s.strip(), status_year_2))

            status_turnover_3 = list(map(lambda s: s.strip(), status_turnover_2))




        ##############################################################
        #  Technology tables
        fatch_info = "select * from technology"
        response = getdata(fatch_info)
        if response.empty:
            text1 = ""
            pages3 = convert_from_path(path + '/documents/technology1.pdf', 500)[0]
            # for page in pages3:
            pages3.save(path + '/converted_png/Experience.png', 'png')
            #     # third file extract
            file3 = path + '/converted_png/Experience.png'
            #     # Parse data from file
            file_data3 = parser.from_file(file3)
            # Get files text content
            text1 += file_data3['content']

            technology_text1_pdf1 = ''
            technology_pdf_text = re.finditer(r'(\bConsultancy Services\b)((.*\n){10})', str(text1))
            for client_txt in technology_pdf_text:
                technology_text1_pdf1 = client_txt.group()
                technology_text1_pdf1 = technology_text1_pdf1.replace("'", "")

            technology_keyword1 = ''
            keyword_pdf_text = re.finditer(r'(LiDAR|GPR|CADASTRAL MAPS)((.*\n){3})', str(text1))
            for client_txt in keyword_pdf_text:
                technology_keyword1 = client_txt.group()

            undertaking_text1 = ''
            annual_turnover_pdf_text = re.finditer(r'(\bundertaking|\bUNDERTAKING\b)(.*)', text1)
            for client_txt in annual_turnover_pdf_text:
                undertaking_text1 = client_txt.group()

            if (undertaking_text1 == ""):
                undertaking_text1 = "Client Certified"




            #####################################
            # secound pdf
            text2 = ""
            pages3 = convert_from_path(path + '/documents/technology2.pdf', 500)[0]
            # for page in pages3:
            pages3.save(path + '/converted_png/Experience.png', 'png')
            #     # third file extract
            file3 = path + '/converted_png/Experience.png'
            #     # Parse data from file
            file_data3 = parser.from_file(file3)
            # Get files text content
            text2 += file_data3['content']

            technology_text2_pdf2 = ''
            technology_pdf_text = re.finditer(r'(\bConsultancy Services\b)((.*\n){7})', str(text2))
            for client_txt in technology_pdf_text:
                technology_text2_pdf2 = client_txt.group()
                technology_text2_pdf2 = technology_text2_pdf2.replace("'", "")


            technology_keyword2 = ''
            keyword_pdf_text = re.finditer(r'(LiDAR|GPR|CADASTRAL MAPS)((.*\n){3})', str(text2))
            for client_txt in keyword_pdf_text:
                technology_keyword2 = client_txt.group()

            undertaking_text2 = ''
            annual_turnover_pdf_text = re.finditer(r'(\bundertaking|\bUNDERTAKING\b)(.*)', text2)
            for client_txt in annual_turnover_pdf_text:
                undertaking_text2 = client_txt.group()

            if (undertaking_text2 == ""):
                undertaking_text2 = "Client Certified"

            #########################################
            ## third pdf

            text3 = ""
            pages3 = convert_from_path(path + '/documents/technology_gpr1.pdf', 500)[0]
            # for page in pages3:
            pages3.save(path + '/converted_png/Experience.png', 'png')
            #     # third file extract
            file3 = path + '/converted_png/Experience.png'
            #     # Parse data from file
            file_data3 = parser.from_file(file3)
            # Get files text content
            text3 += file_data3['content']

            technology_text3_pdf3 = ''
            technology_pdf_text = re.finditer(r'(\bAssignment Name\b)((.*\n){7})', str(text3))
            for client_txt in technology_pdf_text:
                technology_text3_pdf3 = client_txt.group()
                technology_text3_pdf3 = technology_text3_pdf3.lstrip('Assignment Name:')
                technology_text3_pdf3 = technology_text3_pdf3.replace("'", "")


            technology_keyword3 = ''
            keyword_pdf_text = re.finditer(r'(LiDAR|GPR|CADASTRAL MAPS)((.*\n){3})', str(text3))
            for client_txt in keyword_pdf_text:
                technology_keyword3 = client_txt.group()

            undertaking_text3 = ''
            annual_turnover_pdf_text = re.finditer(r'(\bundertaking|\bUNDERTAKING\b)(.*)', text3)
            for client_txt in annual_turnover_pdf_text:
                undertaking_text3 = client_txt.group()

            if (undertaking_text3 == ""):
                undertaking_text3 = "Client Certified"


            ###############################
            ## forth pdf
            text4 = ""
            pages3 = convert_from_path(path + '/documents/technology_caras_maps.pdf', 500)[0]
            # for page in pages3:
            pages3.save(path + '/converted_png/Experience.png', 'png')
            #     # third file extract
            file3 = path + '/converted_png/Experience.png'
            #     # Parse data from file
            file_data3 = parser.from_file(file3)
            # Get files text content
            text4 += file_data3['content']

            technology_text4_pdf4 = ''
            technology_pdf_text = re.finditer(r'(\bAssignment Name\b)((.*\n){10})', str(text4))
            for client_txt in technology_pdf_text:
                technology_text4_pdf4 = client_txt.group()
                technology_text4_pdf4 = technology_text4_pdf4.lstrip('Assignment Name:')
                technology_text4_pdf4 = technology_text4_pdf4.replace("'", "")


            technology_keyword4 = ''
            keyword_pdf_text = re.finditer(r'(LiDAR|GPR|CADASTRAL MAPS)((.*\n){3})', str(text4))
            for client_txt in keyword_pdf_text:
                technology_keyword4 = client_txt.group()

            undertaking_text4 = ''
            annual_turnover_pdf_text = re.finditer(r'(\bundertaking|\bUNDERTAKING\b)(.*)', text4)
            for client_txt in annual_turnover_pdf_text:
                undertaking_text4 = client_txt.group()

            if (undertaking_text4 == ""):
                undertaking_text4 = "Client Certified"


            technology1_given_text = "Consultancy Services for Preparation of revised Detailed Project Report and" \
                                     " Verification of Executed Quantities / items for widening to 2 - Lane of Longleng - " \
                                     "Changtongya Road (35 km), Mon - Tamlu - Merangkong Road (100 km), of (30 km), Phek - " \
                                     "Pfutsero Road (66 km) and Zunheboto - Chakabama Road (128 km) under Phase â€˜Aâ€™ of " \
                                     "SARDP-NE in the state of Nagaland (Total Length: Existing 329 km/ Design 303.4 km)"

            technology2_given_text = "Consultancy Services for Construction of bridge across Hasdeo River for Future " \
                                     "ash dyke of Korba Stage-I & II at Koriyaghat in Korba district in Chhatisgarh" \
                                     " state (Length: 6.00 km)"

            technology3_given_text = "Consultancy Services for Preparation of Detailed Project Report for development" \
                                     " of Economic Corridors, Inter Corridors, Feeder Routes to improve the " \
                                     "efficiency of freight movement in India under Bharatmala Pariyojna " \
                                     "(Lot-2/Haryana/Package-1)"

            technology4_given_text = "Consultancy Services for Preparation of Detailed Project Report for Rehabilitation" \
                                     " and Up gradation of NH stretches under NHDP-IVB (Group â€˜Aâ€™, Package No. " \
                                     "UP/DPR/NHDP-IV/03) in Uttar Pradesh [Varanasi to Hanumanha-NH-7, 125.5 km, " \
                                     "Lucknow to Raibareilly-NH24B, 82 km, Unnao to Lalganj-NH-232A, 68 km] " \
                                     "Total Project length-Awarded 275.5 km/ Designed 261.348 km incl. 133.598 km of " \
                                     "4-lane & 127.75 km of 2-lane (BOT Project)"

            technology_name = ['LiDAR', 'LiDAR', 'GPR', 'CADASTRAL MAPS']
            sno = ['1', '2', '1', '1']
            technology_given_list = [technology1_given_text, technology2_given_text, technology3_given_text, technology4_given_text]
            technology_uploded_list = [technology_text1_pdf1, technology_text2_pdf2, technology_text3_pdf3, technology_text4_pdf4]
            undertaking_text = [undertaking_text1, undertaking_text2, undertaking_text3, undertaking_text4]
            document = ['documents/technology1.pdf',
                        'documents/technology2.pdf',
                        'documents/technology_gpr1.pdf',
                        'documents/technology_caras_maps.pdf'
                        ]
            supporing_document = ['documents/technology1.pdf',
                        'documents/technology2.pdf',
                        'documents/technology_gpr1.pdf',
                        'documents/technology_caras_maps.pdf'
                        ]
            status_val = []
            for j, k in zip(technology_given_list, technology_uploded_list):
                cosine_sim = get_result(j, k)
                if cosine_sim >= 0.5:
                    status_val.append("Matched")
                    status_val.append(k)
                else:
                    status_val.append("Not matched")
                    status_val.append(k)

            status_val1 = status_val[0::2]
            # count_tech_status1 = Counter(status_val1).most_common()

            status_val2 = status_val[1::2]

            status_val3 = list(map(lambda s: s.strip(), status_val2))
            tech_keywords_list = [technology_keyword1, technology_keyword2, technology_keyword3, technology_keyword4]

            technology_zipall = zip(technology_name, sno, technology_given_list, status_val1, document, supporing_document, undertaking_text)
            count_tech_status1 = []
            for technology_name, sno, technology_given_list, status_val1, document, supporing_document, undertaking_text, status_val3, tech_keywords_list in zip(
                    technology_name, sno, technology_given_list, status_val1, document, supporing_document,
                    undertaking_text, status_val3, tech_keywords_list):
                params = {
                    'technology_name': technology_name,
                    'sno': sno,
                    'technology_given_list': technology_given_list,
                    'status_val1': status_val1,
                    'document': document,
                    'supporing_document': supporing_document,
                    'undertaking_text': undertaking_text,
                    'status_val3': status_val3,
                    'tech_keywords_list': tech_keywords_list
                }

                insert_query = "insert into technology(technology_name, sno, technology_given_list, " \
                               " status_val1, document, supporing_document, undertaking_text," \
                               " status_val3, tech_keywords_list)" \
                               + " VALUES('{technology_name}', '{sno}', '{technology_given_list}', " \
                               " '{status_val1}', '{document}', '{supporing_document}', '{undertaking_text}'," \
                               " '{status_val3}', '{tech_keywords_list}')".format(**params)

                cursor = connection.cursor()
                cursor.execute(insert_query)
                fatch_info = "select * from technology"
                response = getdata(fatch_info)
                technology_name = response['technology_name'].values.tolist()
                sno = response['sno'].values.tolist()
                technology_given_list = response['technology_given_list'].values.tolist()
                status_val1 = response['status_val1'].values.tolist()
                document = response['document'].values.tolist()
                supporing_document = response['supporing_document'].values.tolist()
                undertaking_text = response['undertaking_text'].values.tolist()
                status_val3 = response['status_val3'].values.tolist()
                tech_keywords_list = response['tech_keywords_list'].values.tolist()

                count_tech_status1 = Counter(status_val1).most_common()

                technology_zipall = zip(technology_name, sno, technology_given_list, status_val1, document,
                                        supporing_document, undertaking_text)
        else:
            fatch_info = "select * from technology"
            response = getdata(fatch_info)
            technology_name = response['technology_name'].values.tolist()
            sno = response['sno'].values.tolist()
            technology_given_list = response['technology_given_list'].values.tolist()
            status_val1 = response['status_val1'].values.tolist()
            document = response['document'].values.tolist()
            supporing_document = response['supporing_document'].values.tolist()
            undertaking_text = response['undertaking_text'].values.tolist()
            status_val3 = response['status_val3'].values.tolist()
            tech_keywords_list = response['tech_keywords_list'].values.tolist()

            count_tech_status1 = Counter(status_val1).most_common()

            technology_zipall = zip(technology_name, sno, technology_given_list, status_val1, document,
                                    supporing_document, undertaking_text)

        # # #         #######################################################################
# # #         # Work Detail 1 #######
# # #
        fatch_info = "select * from work_detail"
        response = getdata(fatch_info)
        if response.empty:
            text3 = ""
            pages3 = convert_from_path(path + '/documents/work_detail4.pdf', 500)[0]
            # for page in pages3:
            pages3.save(path + '/converted_png/Experience.png', 'png')
            #     # third file extract
            file3 = path + '/converted_png/Experience.png'
            #     # Parse data from file
            file_data3 = parser.from_file(file3)
            # Get files text content
            text3 += file_data3['content']

            client_text2 = ""
            client2 = re.finditer(r'(.*)(\bConsultancy \(PMC\) Services\b)((.*\n){4})', str(text3))
            for client_txt in client2:
                client_text2 = client_txt.group()

            project_length2 = ""
            p_length2 = re.finditer(r'(\bTotal length\b)(.*)?km', str(text3))
            for client_txt in p_length2:
                project_length2 = client_txt.group()
                project_length2 = project_length2.lstrip('Total length')

            cost_text2 = ""
            cost2 = re.finditer(r'\bCost of Project\b(.*)(?!crore)', str(text3))
            for cost_txt in cost2:
                cost_text2 = cost_txt.group()
                cost_text2 =  cost_text2.lstrip('Cost of Project:Rs.')
                cost_text2 = re.findall("\d+\.\d+", cost_text2)
                cost_text2 = round(float(cost_text2[0]))

            Consultancy_Fees = ""
            c_fees = re.finditer(r'(\bConsultancy Fee\b)(.*)', str(text3))
            for cost_txt in c_fees:
                Consultancy_Fees = cost_txt.group()
                Consultancy_Fees = Consultancy_Fees.lstrip('Consultancy Fee: Rs.')
                Consultancy_Fees = re.findall("\d+\.\d+", Consultancy_Fees)
                Consultancy_Fees = Consultancy_Fees[0]
                Consultancy_Fees = float(Consultancy_Fees) * 100

            Fees_Received_by_Firm = ""
            firm_fees = re.finditer(r'(?i)\d+(.?\d+)+\s?(Lac|lakh|l|lacs|lakhs|\(lac\))', str(text3))
            for cost_txt in firm_fees:
                Fees_Received_by_Firm = cost_txt.group()

            Work_Duration = ""
            s_date2 = re.finditer(r'(\bDate of Commencement:\b)((.*\n){3})', str(text3))
            for client_txt in s_date2:
                Work_Duration = client_txt.group()
                Work_Duration = Work_Duration.lstrip('Date of Commencement:')
                Work_Duration = Work_Duration.lstrip('Date of Completion:')

            Terrain_Work = ""
            t_work = re.finditer(r'(?i)\d+(.?\d+)+\s?(.*)(\bLane|Hill|Plain\b)(.*)', str(text3))
            for cost_txt in t_work:
                Terrain_Work = cost_txt.group()

            Role_Of_Firm = ""
            role_f = re.finditer(r'(\bSole Firm\b)(.*)', str(text3))
            for client_txt in role_f:
                Role_Of_Firm = client_txt.group()

            Type_of_Assignment = ""
            assignment = re.finditer(r'(\bSupervision(EPC)\b)(.*)', str(text3))
            for client_txt in assignment:
                Type_of_Assignment = client_txt.group()

            undertaking_text1 = ''
            annual_turnover_pdf_text = re.finditer(r'(\bundertaking|\bUNDERTAKING\b)(.*)', text3)
            for client_txt in annual_turnover_pdf_text:
                undertaking_text1 = client_txt.group()

            if (undertaking_text1 == ""):
                undertaking_text1 = "Client Certified"


            Name_of_Work1 = "Project Management Consultancy (PMC) services for construction works related to widening/ " \
                           "modification of road to create multiple lanes (8-Lanes) of existing carriage way for " \
                           "High Capacity Bus System (HCBS) â€“ subsequently renamed as Bus Rapid Transit (BRT) System" \
                           " on Ambedkar Nagar to Delhi Gate section (Length: 14.5 km) in Delhi."

            colomn_headr = ['Name of Work', 'Length Of Project', 'Project Cost (Cr)', 'Consultancy Fees (Lac)',
                            'Fees Received by Firm (Lac)', 'Work Duration', 'Terrain Work', 'Role Of Firm',
                            'Type of Assignment']

            work_detail_text_given = [Name_of_Work1, "14.50", "216.00", "600.00", "600.00", "05/09/2006 to 06/07/2010",
                                      "2 Lane= 0 4 Lane=0 6 Lane=14.5 Hill=0 Plain=0 Hill=0 Plain=0 Hill=0 Plain=14.5",
                                      "Sole Firm", "Supervision(EPC)"
                                      ]
            work_detail_text_matched = [client_text2, project_length2, str(cost_text2), str(Consultancy_Fees), Fees_Received_by_Firm,
                                       Work_Duration, Terrain_Work, Role_Of_Firm, "Supervision(EPC)"]
            status_val = []



            for j, k in zip(work_detail_text_given, work_detail_text_matched):
                cosine_sim = get_result(j, k)
                if cosine_sim >= 0.3:
                    status_val.append("Matched")
                else:
                    status_val.append("Not matched")

            detail_work_zipall = zip(colomn_headr, work_detail_text_given, work_detail_text_matched, status_val)

            # ###########################
            # workdetail 2

            text3 = ""
            pages3 = convert_from_path(path + '/documents/file2.pdf', 500)[0]
            # for page in pages3:
            pages3.save(path + '/converted_png/Experience.png', 'png')
            #     # third file extract
            file3 = path + '/converted_png/Experience.png'
            #     # Parse data from file
            file_data3 = pytesseract.image_to_string(Image.open(file3), lang='eng')
            # Get files text content
            text3 += file_data3

            client_text2 = ""
            client2 = re.finditer(r'(.*)(\bconsultancy services\b)((.*\n){10})', text3)
            for client_txt in client2:
                client_text2 = client_txt.group()

            project_length2 = ""
            p_length2 = re.finditer(r'(\bProject road length\b)(.*)?kms', text3)
            for client_txt in p_length2:
                project_length2 = client_txt.group()
                project_length2 = project_length2.lstrip('Project road length')
                project_length2 = re.findall("\d+\.\d+", project_length2)
                project_length2 = project_length2[0]
                project_length2 = "%.2f" % float(project_length2)

            cost_text2 = ""
            cost2 = re.finditer(r'\bContract Value INR\b(.*)(?!crores)', text3)
            for cost_txt in cost2:
                cost_text2 = cost_txt.group()
                cost_text2 = cost_text2.lstrip('Contract Value INR =')
                cost_text2 = [int(s) for s in cost_text2.split() if s.isdigit()]
                # cost_text2 = re.findall("\d+\.\d+", cost_text2)
                cost_text2 = cost_text2[0]
                # cost_text2 = round(float(cost_text2[0]))

            Consultancy_Fees = ""
            c_fees = re.finditer(r'(\bConsultancy Fee\b)(.*)', text3)
            for cost_txt in c_fees:
                Consultancy_Fees = cost_txt.group()
                Consultancy_Fees = Consultancy_Fees.lstrip('Consultancy Fee: Rs.')
                Consultancy_Fees = re.findall("\d+\.\d+", Consultancy_Fees)
                Consultancy_Fees = Consultancy_Fees[0]
                Consultancy_Fees = float(Consultancy_Fees) * 100

            Fees_Received_by_Firm = ""

            firm_fees = re.finditer(r'(\bFees Received by Firm (Lac)\b)(.*)', text3)
            for cost_txt in firm_fees:
                Fees_Received_by_Firm = cost_txt.group()

            Work_Duration = ""
            s_date2 = re.finditer(r'(\bDate of Commencement:\b)((.*\n){3})', text3)
            for client_txt in s_date2:
                Work_Duration = client_txt.group()
                Work_Duration = Work_Duration.lstrip('Date of Commencement:')
                Work_Duration = Work_Duration.lstrip('Date of Completion:')

            Terrain_Work = ""
            t_work = re.finditer(r'(?i)\d+(.?\d+)+\s?(.*)(\bLane|Hill|Plain\b)(.*)', text3)
            for cost_txt in t_work:
                Terrain_Work = cost_txt.group()

            Role_Of_Firm = ""
            role_f = re.finditer(r'(\bLead Member\b)(.*)', text3)
            for client_txt in role_f:
                Role_Of_Firm = client_txt.group()

            Type_of_Assignment = ""
            assignment = re.finditer(r'(\bSupervision(EPC)\b)(.*)', text3)
            for client_txt in assignment:
                Type_of_Assignment = client_txt.group()

            undertaking_text2 = ''
            annual_turnover_pdf_text = re.finditer(r'(\bundertaking|\bUNDERTAKING\b)(.*)', text3)
            for client_txt in annual_turnover_pdf_text:
                undertaking_text2 = client_txt.group()

            if (undertaking_text2 == ""):
                undertaking_text2 = "Client Certified"

            Name_of_Work2 = "Independent Engineer for 8/6- Laning & strengthening of the Existing 6/4 lane stretch " \
                           "from km 14.3 to km 42.0 of NH-8 (Length: 27.7 km) on BOT basis in the State " \
                           "of Haryana and NCR of Delhi."

            colomn_headr = ['Name of Work', 'Length Of Project', 'Project Cost (Cr)', 'Consultancy Fees (Lac)',
                            'Fees Received by Firm (Lac)', 'Work Duration', 'Terrain Work', 'Role Of Firm',
                            'Type of Assignment']
            # work_detail_text_given = [Name_of_Work, "14.50", "216.00", "600.00", "600.00", "05/09/2006 to 06/07/2010",
            #                           "2 Lane= 0 4 Lane=0 6 Lane=14.5 Hill=0 Plain=0 Hill=0 Plain=0 Hill=0 Plain=14.5",
            #                           "Sole Firm", "Supervision(EPC)"
            #                           ]
            work_detail_text_given2 = [Name_of_Work2, "27.70", "700.00", "563.00", "1,625.00", "01/06/2002 to 31/12/2008",
                                      "2 Lane= 0 4 Lane=0 6 Lane=27.7 Hill=0 Plain=0 Hill=0 Plain=0 Hill=0 Plain=27.7",
                                      "Lead Member", "Supervision(PPP)"
                                     ]



            work_detail_text_matched2 = [client_text2, str(project_length2), str(cost_text2), str(Consultancy_Fees),
                                        Fees_Received_by_Firm,
                                        Work_Duration, Terrain_Work, Role_Of_Firm, "Supervision(PPP)"]
            status_val1 = []
            for j, k in zip(work_detail_text_given, work_detail_text_matched):
                cosine_sim = get_result(j, k)
                if cosine_sim >= 0.3:
                    status_val1.append("Matched")
                    status_val1.append(k)
                else:
                    status_val1.append("Not matched")
                    status_val1.append(k)
            status_val2 = []
            for j, k in zip(work_detail_text_given2, work_detail_text_matched2):
                cosine_sim = get_result(j, k)
                if cosine_sim >= 0.3:
                    status_val2.append("Matched")
                    status_val2.append(k)
                else:
                    status_val2.append("Not matched")
                    status_val2.append(k)

            first_pdf_text_status = status_val1[0::2]
            # count_work_detail_status1 = Counter(first_pdf_text_status).most_common()

            secound_pdf_text_status = status_val2[0::2]

            # count_work_detail_status2 = Counter(secound_pdf_text_status).most_common()
            status_txt_1 = status_val1[1::2]
            status_txt_2 = status_val2[1::2]

            status_txt3_1 = list(map(lambda s: s.strip(), status_txt_1))

            status_txt3_2 = list(map(lambda s: s.strip(), status_txt_2))


            sno = ['12','Status', '2', 'Status']
            name_of_work = [Name_of_Work1, first_pdf_text_status[0], Name_of_Work2, secound_pdf_text_status[0]]
            length_project = ["14.50",first_pdf_text_status[1], "27.70", secound_pdf_text_status[1]]
            project_cost = ["216.00",first_pdf_text_status[2], "700.00", secound_pdf_text_status[2]]
            consultancy_fee = ["600.00",first_pdf_text_status[3], "563.00", secound_pdf_text_status[3]]
            fee_recived_by_firm = ["600.00",first_pdf_text_status[4], "1,625.00", secound_pdf_text_status[4]]
            work_doretion = ["05/09/2006 to 06/07/2010",first_pdf_text_status[5], "01/06/2002 to 31/12/2008", secound_pdf_text_status[5]]
            terrain_work = ["2 Lane= 0 4 Lane=0 6 Lane=14.5 \n Hill=0 Plain=0 Hill=0 \n Plain=0 Hill=0 Plain=14.5",
                            first_pdf_text_status[6],
                            "2 Lane= 0 4 Lane=0 6 Lane=27.7 \n Hill=0 Plain=0 Hill=0 \n Plain=0 Hill=0 Plain=27.7",
                            secound_pdf_text_status[6]
                            ]
            role_of_firm = ["Sole Firm",first_pdf_text_status[7], "Lead Member", secound_pdf_text_status[7]]
            type_of_assingment = ["Supervision(EPC)",first_pdf_text_status[8], "Supervision(PPP)", secound_pdf_text_status[8]]

            document = ['documents/work_detail4.pdf',
                        '',
                        'documents/file2.pdf',
                        ''
                        ]
            undertaking_text = [undertaking_text1, '', undertaking_text2, '']
            detail_work_zipall1 = zip(sno, name_of_work, length_project, project_cost, consultancy_fee,
                                      fee_recived_by_firm, work_doretion,terrain_work, role_of_firm, type_of_assingment,
                                      document
                                      )
            status_work_detail = zip(status_val1, status_val2)
            count_work_detail_status1 = []
            count_work_detail_status2 = []
            for (sno, name_of_work, length_project, project_cost, consultancy_fee, fee_recived_by_firm,
                 work_doretion,terrain_work, role_of_firm, type_of_assingment, document, status_txt3_1,
                 status_txt3_2, first_pdf_text_status, secound_pdf_text_status) in zip(
                    sno, name_of_work, length_project, project_cost, consultancy_fee, fee_recived_by_firm, work_doretion,
                    terrain_work, role_of_firm, type_of_assingment, document,
                    status_txt3_1, status_txt3_2, first_pdf_text_status, secound_pdf_text_status):
                params = {
                    'sno': sno,
                    'name_of_work': name_of_work,
                    'length_project': length_project,
                    'project_cost': project_cost,
                    'consultancy_fee': consultancy_fee,
                    'fee_recived_by_firm': fee_recived_by_firm,
                    'work_doretion': work_doretion,
                    'terrain_work': terrain_work,
                    'role_of_firm': role_of_firm,
                    'type_of_assingment': type_of_assingment,
                    'document': document,
                    'status_txt3_1': status_txt3_1,
                    'status_txt3_2': status_txt3_2,
                    'first_pdf_text_status': first_pdf_text_status,
                    'secound_pdf_text_status': secound_pdf_text_status
                }

                insert_query = "insert into work_detail(sno, name_of_work, length_project, project_cost, " \
                               "consultancy_fee, fee_recived_by_firm, work_doretion, " \
                               "terrain_work, role_of_firm, type_of_assingment, document, " \
                               "status_txt3_1, status_txt3_2, first_pdf_text_status, secound_pdf_text_status)" \
                               + " VALUES('{sno}', '{name_of_work}', '{length_project}', '{project_cost}', '{consultancy_fee}', " \
                                 "'{fee_recived_by_firm}', '{work_doretion}', '{terrain_work}', '{role_of_firm}'," \
                                 " '{type_of_assingment}', '{document}', '{status_txt3_1}', '{status_txt3_2}'," \
                                 " '{first_pdf_text_status}', '{secound_pdf_text_status}')".format(**params)

                cursor = connection.cursor()
                cursor.execute(insert_query)
                fatch_info = "select * from work_detail"
                response = getdata(fatch_info)
                sno = response['sno'].values.tolist()
                name_of_work = response['name_of_work'].values.tolist()
                length_project = response['length_project'].values.tolist()
                project_cost = response['project_cost'].values.tolist()
                consultancy_fee = response['consultancy_fee'].values.tolist()
                fee_recived_by_firm = response['fee_recived_by_firm'].values.tolist()
                work_doretion = response['work_doretion'].values.tolist()
                terrain_work = response['terrain_work'].values.tolist()
                role_of_firm = response['role_of_firm'].values.tolist()
                type_of_assingment = response['type_of_assingment'].values.tolist()
                document = response['document'].values.tolist()
                status_txt3_1 = response['status_txt3_1'].values.tolist()
                status_txt3_2 = response['status_txt3_2'].values.tolist()
                first_pdf_text_status = response['first_pdf_text_status'].values.tolist()
                secound_pdf_text_status = response['secound_pdf_text_status'].values.tolist()

                detail_work_zipall1 = zip(sno, name_of_work, length_project, project_cost, consultancy_fee,
                                          fee_recived_by_firm, work_doretion, terrain_work, role_of_firm,
                                          type_of_assingment,
                                          document
                                          )
                count_work_detail_status1 = Counter(first_pdf_text_status).most_common()
                count_work_detail_status2 = Counter(secound_pdf_text_status).most_common()
        else:
            fatch_info = "select * from work_detail"
            response = getdata(fatch_info)
            sno = response['sno'].values.tolist()
            name_of_work = response['name_of_work'].values.tolist()
            length_project = response['length_project'].values.tolist()
            project_cost = response['project_cost'].values.tolist()
            consultancy_fee = response['consultancy_fee'].values.tolist()
            fee_recived_by_firm = response['fee_recived_by_firm'].values.tolist()
            work_doretion = response['work_doretion'].values.tolist()
            terrain_work = response['terrain_work'].values.tolist()
            role_of_firm = response['role_of_firm'].values.tolist()
            type_of_assingment = response['type_of_assingment'].values.tolist()
            document = response['document'].values.tolist()
            status_txt3_1 = response['status_txt3_1'].values.tolist()
            status_txt3_2 = response['status_txt3_2'].values.tolist()
            first_pdf_text_status = response['first_pdf_text_status'].values.tolist()
            secound_pdf_text_status = response['secound_pdf_text_status'].values.tolist()

            detail_work_zipall1 = zip(sno, name_of_work, length_project, project_cost, consultancy_fee,
                                      fee_recived_by_firm, work_doretion, terrain_work, role_of_firm,
                                      type_of_assingment,
                                      document
                                      )
            count_work_detail_status1 = Counter(first_pdf_text_status).most_common()
            count_work_detail_status2 = Counter(secound_pdf_text_status).most_common()

        return render(request, 'index.html', {
            'year_list_given': year_list_given,
            'tunover_list_given': tunover_list_given,
            'turn_over_zipall': turn_over_zipall,
            'annual_detail_year_list': annual_detail_year_list,
            'status_year_2': status_year_3,
            'status_turnover_2': status_turnover_3,
            'count_turnover_year_status1': count_turnover_year_status1,
            'count_turnover_status2': count_turnover_status2,
            'technology_given_list': technology_given_list,
            'technology_zipall': technology_zipall,
            'status_val3': status_val3,
            'count_tech_status1': count_tech_status1,
            'tech_keywords_list': tech_keywords_list,
            'detail_work_zipall1': detail_work_zipall1,
            'status_txt3_1': status_txt3_1,
            'status_txt3_2': status_txt3_2,
            'count_work_detail_status1': count_work_detail_status1,
            'count_work_detail_status2': count_work_detail_status2

        })

    except Exception as e:
        print("=========", e)
    return render(request, 'index.html',)


def company_two_view(request):
    """

    :param request:
    :return:
    """
    # if request.method == 'POST':
    try:
        text1 = ""
        pages3 = convert_from_path(path + '/documents/voyant_turnover_2015_16.pdf', 500)[0]
        # for page in pages3:
        pages3.save(path + '/converted_png/Experience.png', 'png')
    #     # third file extract
        file3 = path + '/converted_png/Experience.png'
    #     # Parse data from file
        file_data3 = parser.from_file(file3)
        # Get files text content
        text1 += file_data3['content']

        turnover_text1_pdf1 = ''
        annual_turnover_year_pdf_text = re.finditer(r'(\b2015-16\b)', str(text1))
        for client_txt in annual_turnover_year_pdf_text:
            turnover_text1_pdf1 = client_txt.group()
        turnover_text2_pdf1 = ''
        annual_turnover_pdf_text = re.finditer(r'(\bTotal Liabilities\b)(.*)', str(text1))
        for client_txt in annual_turnover_pdf_text:
            turnover_text2_pdf1 = client_txt.group()

        #####################################
        # secound pdf

        text2 = ""
        pages3 = convert_from_path(path + '/documents/voyant_turnover_2017_18.pdf', 500)[0]
        # for page in pages3:
        pages3.save(path + '/converted_png/Experience.png', 'png')
        #     # third file extract
        file3 = path + '/converted_png/Experience.png'
        #     # Parse data from file
        file_data3 = parser.from_file(file3)
        # Get files text content
        text2 += file_data3['content']
        turnover_text1_pdf2 = ''
        annual_turnover_year_pdf_text = re.finditer(r'(\bYEAR|year\b)(.*)', str(text2))
        for client_txt in annual_turnover_year_pdf_text:
            turnover_text1_pdf2 = client_txt.group()
        turnover_text2_pdf2 = ''
        annual_turnover_pdf_text = re.finditer(r'(\bYears\b)((.*\n){3})', str(text2))
        for client_txt in annual_turnover_pdf_text:
            turnover_text2_pdf2 = client_txt.group()
            turnover_text2_pdf2 = turnover_text2_pdf2.lstrip('Years \n Years')
            turnover_text2_pdf2 = turnover_text2_pdf2.replace("84.17 55.91 231.89 77.30", "")

        client_list_given = ['Voyants Solutions', 'Voyants Solutions']
        year_list_given = ['2015-16 [Calendar Year:2016]', '2017-18 [Calendar Year:2017]', ]
        tunover_list_given = ['56.67', '91.81']

        annual_detail_work_given_list = ["2015-16 [Calendar Year:2016]", "2017-18 [Calendar Year:2017]", "56.67",
                                         "91.81"]
        annual_detail_work_uploded_list = [turnover_text1_pdf1, turnover_text1_pdf2, turnover_text2_pdf1,
                                           turnover_text2_pdf2]

        annual_detail_year_list = [turnover_text1_pdf1, turnover_text1_pdf2]
        annual_detail_turnover_list = [turnover_text2_pdf1, turnover_text2_pdf2]


        status_year = []
        status_turnover = []
        for j, k in zip(year_list_given, annual_detail_year_list):
            cosine_sim = get_result(j, k)
            if cosine_sim >= 0.4:
                status_year.append("Matched")
            else:
                status_year.append("Not matched")
        for j, k in zip(tunover_list_given, annual_detail_turnover_list):
            cosine_sim = get_result(j, k)
            if cosine_sim >= 0.4:
                status_turnover.append("Matched")
            else:
                status_turnover.append("Not matched")
        turn_over_zipall = zip(client_list_given, year_list_given,
                               tunover_list_given, annual_detail_year_list, annual_detail_turnover_list, status_year, status_turnover)

        ###############################################################
        #  Technology tables

        text1 = ""
        pages3 = convert_from_path(path + '/documents/voyant_technology_11.pdf', 500)[0]
        # for page in pages3:
        pages3.save(path + '/converted_png/Experience.png', 'png')
        #     # third file extract
        file3 = path + '/converted_png/Experience.png'
        #     # Parse data from file
        file_data3 = parser.from_file(file3)
        # Get files text content
        text1 += file_data3['content']

        technology_text1_pdf1 = ''
        technology_pdf_text = re.finditer(r'(\bConsultancy Services\b)((.*\n){7})', str(text1))
        for client_txt in technology_pdf_text:
            technology_text1_pdf1 = client_txt.group()

        # #####################################
        # secound pdf
        text2 = ""
        pages3 = convert_from_path(path + '/documents/voyant_technology_13.pdf', 500)[0]
        # for page in pages3:
        pages3.save(path + '/converted_png/Experience.png', 'png')
        #     # third file extract
        file3 = path + '/converted_png/Experience.png'
        #     # Parse data from file
        file_data3 = parser.from_file(file3)
        # Get files text content
        text2 += file_data3['content']
        #
        technology_text2_pdf2 = ''
        technology_pdf_text = re.finditer(r'(\bConsultancy Services\b)((.*\n){7})', str(text2))
        for client_txt in technology_pdf_text:
            technology_text2_pdf2 = client_txt.group()

        #########################################
        # third pdf

        text3 = ""
        pages3 = convert_from_path(path + '/documents/voyant_technology_gpr7.pdf', 500)[0]
        # for page in pages3:
        pages3.save(path + '/converted_png/Experience.png', 'png')
        #     # third file extract
        file3 = path + '/converted_png/Experience.png'
        #     # Parse data from file
        file_data3 = parser.from_file(file3)
        # Get files text content
        text3 += file_data3['content']

        technology_text3_pdf3 = ''
        technology_pdf_text = re.finditer(r'(\bSub: Consultancy Services\b)((.*\n){7})', str(text3))
        for client_txt in technology_pdf_text:
            technology_text3_pdf3 = client_txt.group()
            technology_text3_pdf3 = technology_text3_pdf3.lstrip('Sub: Consultancy Services')

        ###############################
        # forth pdf

        text4 = ""
        pages3 = convert_from_path(path + '/documents/voyant_technology_cors_maps1.pdf', 500)[0]
        # for page in pages3:
        pages3.save(path + '/converted_png/Experience.png', 'png')
        #     # third file extract
        file3 = path + '/converted_png/Experience.png'
        #     # Parse data from file
        file_data3 = parser.from_file(file3)
        # Get files text content
        text4 += file_data3['content']

        technology_text4_pdf4 = ''
        technology_pdf_text = re.finditer(r'(\bAssignment Name\b)((.*\n){7})', str(text4))
        for client_txt in technology_pdf_text:
            technology_text4_pdf4 = client_txt.group()
            technology_text4_pdf4 = technology_text4_pdf4.lstrip('Assignment Name:')

        technology1_given_text = "Consultancy Services for Project Management Phase I including preparation of" \
                                 " Detailed Project Report for Rehabilitation and up-gradation to Two/Four lane " \
                                 "with paved shoulder under Corridor Scheme on EPC Basis of NH/CE/PWD/01 " \
                                 "(Jaisingh Nagar (CG/MP Border)-Kotadol, Ramgarh â€“ Semaria Road of tentative " \
                                 "length 65 Km"

        technology2_given_text = "Consultancy Services for Project Management Phase I including preparation " \
                                 "of Detailed Project Report for Rehabilitation and up-gradation to Two/Four" \
                                 " lane with paved shoulder under Corridor Scheme on EPC Basis of NH/CE/PWD/01 " \
                                 "(Jaisingh Nagar (CG/MP Border)-Kotadol, Ramgarh â€“ Semaria Road of tentative " \
                                 "length 65 Km"

        technology3_given_text = "Consultancy Services for Preparation of Detail Project Report for Proposed Route of New National" \
                                 " Highway starting from (A) Chandil (on NH-18) in Jharkhand- Patamda-Banduan-Jhilimili-" \
                                 " Ranibandh-Khatra- Indpur-Bankura, (B) Purulia-Chandankiyari- Jharia-Dhanbad Road and " \
                                 "(C) Ranchi-Tatisilwari-Sili- Muri_Jhalda-Jaipur and ending at junction with NH-18 in" \
                                 " West Bengal (as per Annexure I ), with a minimum of 2-Lane with Paved Shoulder " \
                                 "Configuration in the District of Bankura & Purulia, in West Bengal Length - 143 Km"

        technology4_given_text = "Feasibility-Cum-Preliminary Design for 4-laning of existing 2-lane National" \
                                 " Highway No. 17 from chainage 161/00 (Kashedi) to 260/000 (Sangmeshwar) of " \
                                 "NH-17 to be executed as BOT (Toll) project on DBFOT pattern " \
                                 "(100.10 km, 4 Lane)"

        technology_name = ['LiDAR', 'LiDAR', 'GPR', 'CADASTRAL MAPS']
        technology_given_list = [technology1_given_text, technology2_given_text, technology3_given_text, technology4_given_text]
        technology_uploded_list = [technology_text1_pdf1, technology_text2_pdf2, technology_text3_pdf3, technology_text4_pdf4]
        status_val = []
        for j, k in zip(technology_given_list, technology_uploded_list):
            cosine_sim = get_result(j, k)
            if cosine_sim >= 0.5:
                status_val.append("Matched")
            else:
                status_val.append("Not matched")
        technology_zipall = zip(technology_name, technology_given_list, technology_uploded_list, status_val)

        # #######################################################################
        # # Work Detail #######
        #
        text3 = ""
        pages3 = convert_from_path(path + '/documents/voyant_work_detail33.pdf', 500)[0]
        # for page in pages3:
        pages3.save(path + '/converted_png/Experience.png', 'png')
        #     # third file extract
        file3 = path + '/converted_png/Experience.png'
        #     # Parse data from file
        file_data3 = parser.from_file(file3)
        # Get files text content
        text3 += file_data3['content']

        #
        client_text2 = ""
        client2 = re.finditer(r'(.*)(\bPROJECT TITLE\b)((.*\n){3})', str(text3))
        for client_txt in client2:
            client_text2 = client_txt.group()
            client_text2 = client_text2.lstrip('PROJECT TITLE')
        #
        project_length2 = ""
        p_length2 = re.finditer(r'(\bPROJECT LENGTH\b)(.*)?km', str(text3))
        for client_txt in p_length2:
            project_length2 = client_txt.group()
            project_length2 = project_length2.lstrip('PROJECT LENGTH')
        #
        cost_text2 = ""
        cost2 = re.finditer(r'(\b4. Contract Sum: \b)(.*)', str(text3))
        for cost_txt in cost2:
            cost_text2 = cost_txt.group()
            # cost_text2 =  cost_text2.lstrip('4. Contract Sum: ')
            cost_text2 = re.findall("\d+\.\d+", cost_text2)
            cost_text2 = round(float(cost_text2[0]))
        #
        Consultancy_Fees = ""
        c_fees = re.finditer(r'(\bConsultancy Fee\b)(.*)', str(text3))
        for cost_txt in c_fees:
            Consultancy_Fees = cost_txt.group()
            Consultancy_Fees = Consultancy_Fees.lstrip('Consultancy Fee: Rs.')
            Consultancy_Fees = re.findall("\d+\.\d+", Consultancy_Fees)
            Consultancy_Fees = Consultancy_Fees[0]
            Consultancy_Fees = float(Consultancy_Fees) * 100
        #
        Fees_Received_by_Firm = ""
        firm_fees = re.finditer(r'(?i)\d+(.?\d+)+\s?(Lac|lakh|l|lacs|lakhs|\(lac\))', str(text3))
        for cost_txt in firm_fees:
            Fees_Received_by_Firm = cost_txt.group()

        Work_Duration = ""
        s_date2 = re.finditer(r'(\b5. Commencement\b)((.*\n){2})', str(text3))
        for client_txt in s_date2:
            Work_Duration = client_txt.group()
            Work_Duration = Work_Duration.lstrip('5. Commencement')
            Work_Duration = Work_Duration.lstrip('6. Completion Date:')

        Terrain_Work = ""
        t_work = re.finditer(r'(?i)\d+(.?\d+)+\s?(.*)(\bLane|Hill|Plain\b)(.*)', str(text3))
        for cost_txt in t_work:
            Terrain_Work = cost_txt.group()

        Role_Of_Firm = ""
        role_f = re.finditer(r'(\bSole Firm\b)(.*)', str(text3))
        for client_txt in role_f:
            Role_Of_Firm = client_txt.group()

        Type_of_Assignment = ""
        assignment = re.finditer(r'(\bFeasibility and Preliminary Study\b)(.*)', str(text3))
        for client_txt in assignment:
            Type_of_Assignment = client_txt.group()

        Name_of_Work = "Contract for Transaction Advisory Services for Viability Study and Engineering" \
                       " Designs for Lagos- Iseyin-Kishi-Kaiama Road in Lagos, Oyo Kwara States" \
                       " â€“ A PPP Project"
        colomn_headr = ['Name of Work', 'Length Of Project', 'Project Cost (Cr)', 'Consultancy Fees (Lac)',
                        'Fees Received by Firm (Lac)', 'Work Duration', 'Terrain Work', 'Role Of Firm',
                        'Type of Assignment']
        work_detail_text_given = [Name_of_Work, "414.40", "3,832.00", "230.66", "90.50", "28/03/2011 to 25/01/2012",
                                  "2 Lane=414.4 4Lane=397.36 6 Lane=18.88 Hill=0 Plain=414.4 Hill=0 Plain=397.36 Hill=0 Plain=18.88",
                                  "Sole Firm", "Feasibility and Preliminary Study"
                                  ]
        work_detail_text_matched = [client_text2, project_length2, str(cost_text2), str(Consultancy_Fees), Fees_Received_by_Firm,
                                   Work_Duration, Terrain_Work, Role_Of_Firm, Type_of_Assignment]
        status_val = []

        for j, k in zip(work_detail_text_given, work_detail_text_matched):
            cosine_sim = get_result(j, k)
            if cosine_sim >= 0.3:
                status_val.append("Matched")
            else:
                status_val.append("Not matched")

        detail_work_zipall = zip(colomn_headr, work_detail_text_given, work_detail_text_matched, status_val)

        return render(request, 'company_two.html', {
            'detail_work_zipall': detail_work_zipall,
            'turn_over_zipall': turn_over_zipall,
            'technology_zipall': technology_zipall
        })

    except Exception as e:
        print("=========", e)
    return render(request, 'company_two.html',)


# def model_form_upload(request):
#     if request.method == 'POST':
#         form = DocumentForm(request.POST, request.FILES)
#         if form.is_valid():
#                 form.save()
#
#                 return redirect('home')
#     else:
#         form = DocumentForm()
#     return render(request, 'model_form_upload.html', {
#         'form': form
#     })
#
#


