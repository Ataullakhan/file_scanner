def get_query(retail_analysis_query):
    """
    :param
    :return:
    """
    switcher = {
        'file_path': """select  document from document;""",

        'detail_work_truncate': """truncate detailworks""",

        'detail_work': """select * from "detailworks" """,

        'detail_work_insert_query': """insert into "detailworks" (detail_work, matched_text_from_given, matched_text_from_uploded, status)
        values ('{detail_work}',
        '{matched_text_from_given}',
        '{matched_text_from_uploded}',
        '{status_val}')""",

        'annual_turnover_truncate': """truncate annualtrunover""",

        'annual_turnover': """select * from annualtrunover """,


        'annualTurnoverValidation': """ select * from "annualTurnoverValidation" """,

        'annualTurnoverValidation_insert_query': """insert into "annualTurnoverValidation" (matched_text_from_given, matched_text_from_uploded, status)
               values (
               '{matched_text_from_given}',
               '{matched_text_from_uploded}',
               '{status_val}')""",

        'annual_turnover_insert_query': """insert into annualtrunover (annual_trunover, matched_text_from_given, matched_text_from_uploded, status)
            values ('{annual_trunover}',
            '{matched_text_from_given}',
            '{matched_text_from_uploded}',
            '{status_val}')""",

        'technology_truncate': """truncate technology""",

        'technology': """select * from technology """,

        'technology_insert_query': """insert into technology (technology, matched_text_from_given, matched_text_from_uploded, status)
                values ('{technology}',
                '{matched_text_from_given}',
                '{matched_text_from_uploded}',
                '{status_val}')""",
    }

    return switcher.get(retail_analysis_query, "nothing")
