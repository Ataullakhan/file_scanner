from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from text_extraction_app import views


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.home, name='home'),
    url(r'^simple$', views.simple_upload, name='simple_upload'),
    url(r'^uploads/compnay_two/$', views.company_two_view, name='compony_two_view'),
    # url(r'^uploads/model/$', views.model_form_upload, name='model_form_upload'),
    url('', include('text_extraction_app.urls'))
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
