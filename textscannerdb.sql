--
-- PostgreSQL database dump
--

-- Dumped from database version 10.7 (Ubuntu 10.7-1.pgdg16.04+1)
-- Dumped by pg_dump version 10.7 (Ubuntu 10.7-1.pgdg16.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: textscanner_user
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO textscanner_user;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: textscanner_user
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO textscanner_user;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: textscanner_user
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: textscanner_user
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO textscanner_user;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: textscanner_user
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO textscanner_user;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: textscanner_user
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: textscanner_user
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO textscanner_user;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: textscanner_user
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO textscanner_user;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: textscanner_user
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: textscanner_user
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO textscanner_user;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: textscanner_user
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO textscanner_user;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: textscanner_user
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO textscanner_user;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: textscanner_user
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: textscanner_user
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO textscanner_user;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: textscanner_user
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: textscanner_user
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO textscanner_user;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: textscanner_user
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO textscanner_user;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: textscanner_user
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: textscanner_user
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO textscanner_user;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: textscanner_user
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO textscanner_user;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: textscanner_user
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: textscanner_user
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO textscanner_user;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: textscanner_user
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO textscanner_user;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: textscanner_user
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: textscanner_user
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO textscanner_user;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: textscanner_user
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO textscanner_user;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: textscanner_user
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: textscanner_user
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO textscanner_user;

--
-- Name: technology; Type: TABLE; Schema: public; Owner: textscanner_user
--

CREATE TABLE public.technology (
    technology_name character varying(700),
    sno character varying(700),
    technology_given_list character varying(700),
    status_val1 character varying(700),
    document character varying(700),
    supporing_document character varying(700),
    undertaking_text character varying(700),
    status_val3 character varying(700),
    tech_keywords_list character varying(700)
);


ALTER TABLE public.technology OWNER TO textscanner_user;

--
-- Name: turnover; Type: TABLE; Schema: public; Owner: textscanner_user
--

CREATE TABLE public.turnover (
    sno character varying(200),
    year_list_given character varying(200),
    tunover_list_given character varying(200),
    status_year_1 character varying(200),
    status_turnover_1 character varying(200),
    document character varying(200),
    supporing_document character varying(200),
    annual_detail_year_list character varying(200),
    status_year_2 character varying(200),
    status_turnover_2 character varying(200)
);


ALTER TABLE public.turnover OWNER TO textscanner_user;

--
-- Name: work_detail; Type: TABLE; Schema: public; Owner: textscanner_user
--

CREATE TABLE public.work_detail (
    sno character varying(200),
    name_of_work character varying(700),
    length_project character varying(700),
    project_cost character varying(700),
    consultancy_fee character varying(700),
    fee_recived_by_firm character varying(700),
    work_doretion character varying(700),
    terrain_work character varying(700),
    role_of_firm character varying(700),
    type_of_assingment character varying(700),
    document character varying(700),
    status_txt3_1 character varying(700),
    status_txt3_2 character varying(700),
    first_pdf_text_status character varying(700),
    secound_pdf_text_status character varying(700)
);


ALTER TABLE public.work_detail OWNER TO textscanner_user;

--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: textscanner_user
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: textscanner_user
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: textscanner_user
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add user	4	add_user
11	Can change user	4	change_user
12	Can delete user	4	delete_user
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: textscanner_user
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: textscanner_user
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: textscanner_user
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: textscanner_user
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: textscanner_user
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: textscanner_user
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2019-06-26 14:38:26.858848+05:30
2	auth	0001_initial	2019-06-26 14:38:27.78828+05:30
3	admin	0001_initial	2019-06-26 14:38:28.025639+05:30
4	admin	0002_logentry_remove_auto_add	2019-06-26 14:38:28.05686+05:30
5	contenttypes	0002_remove_content_type_name	2019-06-26 14:38:28.108951+05:30
6	auth	0002_alter_permission_name_max_length	2019-06-26 14:38:28.142383+05:30
7	auth	0003_alter_user_email_max_length	2019-06-26 14:38:28.185873+05:30
8	auth	0004_alter_user_username_opts	2019-06-26 14:38:28.219573+05:30
9	auth	0005_alter_user_last_login_null	2019-06-26 14:38:28.302276+05:30
10	auth	0006_require_contenttypes_0002	2019-06-26 14:38:28.319019+05:30
11	auth	0007_alter_validators_add_error_messages	2019-06-26 14:38:28.348813+05:30
12	auth	0008_alter_user_username_max_length	2019-06-26 14:38:28.463219+05:30
13	auth	0009_alter_user_last_name_max_length	2019-06-26 14:38:28.507676+05:30
14	sessions	0001_initial	2019-06-26 14:38:29.069284+05:30
15	text_extraction_app	0001_initial	2019-06-26 14:38:29.649293+05:30
16	text_extraction_app	0002_auto_20190626_0654	2019-06-26 14:38:29.694589+05:30
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: textscanner_user
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
\.


--
-- Data for Name: technology; Type: TABLE DATA; Schema: public; Owner: textscanner_user
--

COPY public.technology (technology_name, sno, technology_given_list, status_val1, document, supporing_document, undertaking_text, status_val3, tech_keywords_list) FROM stdin;
LiDAR	1	Consultancy Services for Preparation of revised Detailed Project Report and Verification of Executed Quantities / items for widening to 2 - Lane of Longleng - Changtongya Road (35 km), Mon - Tamlu - Merangkong Road (100 km), of (30 km), Phek - Pfutsero Road (66 km) and Zunheboto - Chakabama Road (128 km) under Phase â€˜Aâ€™ of SARDP-NE in the state of Nagaland (Total Length: Existing 329 km/ Design 303.4 km)	Matched	documents/technology1.pdf	documents/technology1.pdf	Client Certified	Consultancy Services for Preparation of  Country: India\nrevised Detailed Project Report and Veriﬁcation of Executed\n\nQuantities / items for widening to 2 - Lane of Longleng -\n\nChangtongya Road (35 km), Mon - Tamlu - Merangkong Road (100\n\nkm), of (30 km), Phek - Pfutsero Road (66 km) and Zunheboto -\n\nChakabama Road (128 km) under Phase ‘A’ of SARDP-NE in the state	LiDAR.\n\n- Carrying out necessary investigations for pavement design , sub-grade characteristics and strength, investigation of\n
LiDAR	2	Consultancy Services for Construction of bridge across Hasdeo River for Future ash dyke of Korba Stage-I & II at Koriyaghat in Korba district in Chhatisgarh state (Length: 6.00 km)	Matched	documents/technology2.pdf	documents/technology2.pdf	Client Certified	Consultancy Services for Construction of bdge Country: India\nacross Hasdeo River for Future ash dyke of Korba Stage-I & II at\n.quWixagh,a,.t.,.in Korbegistrict in Chhati§garh,..state (LengthLéﬂo Km) ,\n\nLocation within Country: Chhattisgarh Professional Stafmermded by RITESi 7\n................................. . Limited ,	LiDAR, GPR & Induction Locator or better technology and Digitization of Cadastral Maps for land surveys)\n\n \n
GPR	1	Consultancy Services for Preparation of Detailed Project Report for development of Economic Corridors, Inter Corridors, Feeder Routes to improve the efficiency of freight movement in India under Bharatmala Pariyojna (Lot-2/Haryana/Package-1)	Matched	documents/technology_gpr1.pdf	documents/technology_gpr1.pdf	Client Certified	Preparation of Detailed Project Report for  Country: India\ndevelopment of Economic Corridors, Inter Corridors, Feeder Routes to I\n\nimprove the efficiency of freight movement in India under Bharatmala\nPariyojna,(Lot-2/Haryana/Package;1)\n\nLocation within Country: Haryana	LiDAR technology coupled with GPS, Total Station, Auto-Level etc.\n2. Identification of underground utilities using Ground Penetration Radar (GPR)\n3. Traffic surveys using ATCC system\n
CADASTRAL MAPS	1	Consultancy Services for Preparation of Detailed Project Report for Rehabilitation and Up gradation of NH stretches under NHDP-IVB (Group â€˜Aâ€™, Package No. UP/DPR/NHDP-IV/03) in Uttar Pradesh [Varanasi to Hanumanha-NH-7, 125.5 km, Lucknow to Raibareilly-NH24B, 82 km, Unnao to Lalganj-NH-232A, 68 km] Total Project length-Awarded 275.5 km/ Designed 261.348 km incl. 133.598 km of 4-lane & 127.75 km of 2-lane (BOT Project)	Matched	documents/technology_caras_maps.pdf	documents/technology_caras_maps.pdf	Client Certified	Consultancy Services for Preparation of Country: India\n| Detailed Project Report for Rehabilitation and Up gradation of NH\nstretches under NHDP-IVB (Group ‘A, Package No.\nUP/DPRINHDP-IVIO3) in Uttar Pradesh [Varanasi to\n\n‘ Hanumanha-NH-7, 125.5 km, Lucknow to RaibareilIy-NH24B, 82\n\n \n\nkm, Unnao to Lalganj-NH-232A, 68 km]	LiDAR, GPR & Induction Locator or better technology and Digitization of Cadastral Maps for land surveys)\n\n \n
\.


--
-- Data for Name: turnover; Type: TABLE DATA; Schema: public; Owner: textscanner_user
--

COPY public.turnover (sno, year_list_given, tunover_list_given, status_year_1, status_turnover_1, document, supporing_document, annual_detail_year_list, status_year_2, status_turnover_2) FROM stdin;
7	2016-17 Calendar Year:2016	1506.8	Matched	Not matched	documents/riteslimited2016-17_7_1.pdf	N/A	. 31.03.2017 31.03.2016	. 31.03.2017 31.03.2016	1,508.57 
8	2017-18 Calendar Year:2017	1602.58	Matched	Matched	documents/riteslimited2017-18_8_1.pdf	N/A	31.03.2018 31.03.2017	31.03.2018 31.03.2017	1,602.58 
\.


--
-- Data for Name: work_detail; Type: TABLE DATA; Schema: public; Owner: textscanner_user
--

COPY public.work_detail (sno, name_of_work, length_project, project_cost, consultancy_fee, fee_recived_by_firm, work_doretion, terrain_work, role_of_firm, type_of_assingment, document, status_txt3_1, status_txt3_2, first_pdf_text_status, secound_pdf_text_status) FROM stdin;
12	Project Management Consultancy (PMC) services for construction works related to widening/ modification of road to create multiple lanes (8-Lanes) of existing carriage way for High Capacity Bus System (HCBS) â€“ subsequently renamed as Bus Rapid Transit (BRT) System on Ambedkar Nagar to Delhi Gate section (Length: 14.5 km) in Delhi.	14.50	216.00	600.00	600.00	05/09/2006 to 06/07/2010	2 Lane= 0 4 Lane=0 6 Lane=14.5 \n Hill=0 Plain=0 Hill=0 \n Plain=0 Hill=0 Plain=14.5	Sole Firm	Supervision(EPC)	documents/work_detail4.pdf	M/s RITES Ltd. has provided Prbject Management Consultancy (PMC) Services for construction\nworks related to widening/modification of road to create multiple lanes (Eight-lanes) of existing\ncarriage way for High Capacity Bus System (HCBS) — subsequently named as Bus Rapid Transit .\n(BRT) System on Ambedkar Nagar — Delhi Gate section in Delhi, Length: 14.5 km.	M/s RITES Ltd. have provided consultancy services for the “Preparation of Detailed Project Report for Conversion of\n\nDelhi-Gurgaon Section of NH8 into Access Controlled 8/6 Lane Highway from Km 14 300 ( Rao Tula Ram Margin\nDelhi) to Km 42/000 (Gurgaon in Haryana) on BOT basis. (1997 to 2001).\n\n&\n\n“Consultancy Services for Independent Eng’neer for the above project” (May 2002 - On going).\nThe requirement as per the RFP, and the relevant services rendered by RITES are Wated below:	Matched	Matched
Status	Matched	Matched	Matched	Matched	Not matched	Matched	Not matched	Not matched	Matched		f 14.5 km	27.70	Matched	Matched
2	Independent Engineer for 8/6- Laning & strengthening of the Existing 6/4 lane stretch from km 14.3 to km 42.0 of NH-8 (Length: 27.7 km) on BOT basis in the State of Haryana and NCR of Delhi.	27.70	700.00	563.00	1,625.00	01/06/2002 to 31/12/2008	2 Lane= 0 4 Lane=0 6 Lane=27.7 \n Hill=0 Plain=0 Hill=0 \n Plain=0 Hill=0 Plain=27.7	Lead Member	Supervision(PPP)	documents/file2.pdf	216	700	Matched	Matched
Status	Matched	Matched	Matched	Not matched	Not matched	Not matched	Not matched	Not matched	Matched		600.0		Matched	Not matched
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: textscanner_user
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: textscanner_user
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: textscanner_user
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 18, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: textscanner_user
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: textscanner_user
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 1, false);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: textscanner_user
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: textscanner_user
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 1, false);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: textscanner_user
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 6, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: textscanner_user
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 16, true);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: textscanner_user
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: textscanner_user
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: textscanner_user
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: textscanner_user
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: textscanner_user
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: textscanner_user
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: textscanner_user
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: textscanner_user
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: textscanner_user
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: textscanner_user
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: textscanner_user
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: textscanner_user
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: textscanner_user
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk; Type: FK CONSTRAINT; Schema: public; Owner: textscanner_user
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

